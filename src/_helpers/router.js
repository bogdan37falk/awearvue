import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Login from '../views/Login.vue'
import Doctor from '../views/DoctorView.vue'
import Pacient from '../views/PacientView.vue'
import Register from '../views/Register.vue'

/* eslint-disable */ 
Vue.use(Router)

export const router = new Router({
  mode:'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/doc',
      name: 'doc',
      component: Doctor
    },
    {
      path: '/pac',
      name: 'pac',
      component: Pacient
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/loggedOff',
      name: 'loggedOff',
      component: Home
    },

  ]
});


router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  
  const publicPages = ['/home', '/about','/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');
  const loggedInData = JSON.parse(loggedIn);




  if(to.fullPath=="/pac" && loggedIn)
  {
    if(loggedInData.fieldOfPractice=="Surgery")
    {
      console.log("from router.js:This be a doc!")
      return next('/doc');
    }
  }


  if(to.fullPath=="/doc" && loggedIn)
  {
    if(loggedInData.weight==12)
    {
      console.log("from router.js:This be a pac!")
      return next('/pac');
    }
  }

  if(!authRequired && to.fullPath=='/login' && loggedIn && loggedInData.fieldOfPractice=="Surgery")
  {
    return next('/doc');
  }


  if(!authRequired && to.fullPath=='/login' && loggedIn && loggedInData.weight==12)
  {
    return next('/pac');
  }

    
  
  if (authRequired && !loggedIn) {
    return next('/home');
  }
 
  //console.log("From Router.js:"+loggedInData.fieldOfPractice);
 
  next();


})
