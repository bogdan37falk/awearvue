import {HubConnectionBuilder, LogLevel} from '@aspnet/signalr'

export default{
    install(Vue) {
        const connection = new HubConnectionBuilder()
            .withUrl(`${Vue.prototype.$http.defaults.baseURL}/sensor-data-hub`)
            .configureLogging(LogLevel.Information)
            .build()

        const sensorDataHub = new Vue()

        connection.on('SensorDataUpdate', (id, TimeStamp, Temperature, Humidity, Pulse, EKGValue) => {
            sensorDataHub.$emit('sensor-data-update', {id, TimeStamp, Temperature, Humidity, Pulse, EKGValue})
        })

        Vue.prototype.$sensorDataHub = sensorDataHub

        let startedPromise = null
        function start () {
            startedPromise = connection.start().catch(err => {
                // eslint-disable-next-line no-console
                console.error('Failed to connect with hub', err)
                return new Promise((resolve, reject) =>
                    setTimeout(() => start().then(resolve).catch(reject), 5000))
            })
            return startedPromise
        }
        connection.onclose(() => start())

        start()
    }
}