import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import { router } from './_helpers'
import store from './store'
import '@fortawesome/fontawesome-free/css/all.css'
import sensorDataHub from '@/sensor-data-hub'
import axios from 'axios'

Vue.config.productionTip = false

axios.defaults.baseURL = 'https://awearapi.azurewebsites.net/api'
Vue.prototype.$http = axios
Vue.use(sensorDataHub)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
